--- EN ---
Hi,

STL   files are for printing purpose, open them with sclic3r, cura, matter control, etc
RSDOC files are for 3D drawing, open them with DesignSpark Mechanical (free, needs to be online when starting the software)

To get updated models and tons of new free models please check our thingiverse page:
http://www.thingiverse.com/3DModularSystems/designs

Scalar 3d printers:
http://www.thingiverse.com/thing:891409

--- FR ---

STL   fichiers pour impression 3D, s'ouvre avec sclic3r, cura, matter control, etc
RSDOC fichiers pour dessin 3D, s'ouvre avec DesignSpark Mechanical (gratuit, il faut �tre connect� � Internet pour le lancer)

Pour avoir les tout derniers mod�les � jours ainsi qu'une tonne de nouveaux mod�les gratuits, consultez notre page thingiverse:
http://www.thingiverse.com/3DModularSystems/designs

Imprimante 3D Scalar:
http://www.thingiverse.com/thing:891409