M190 S50.000000
M109 S60.000000
G21        ;metric values
G90        ;absolute positioning
M82        ;set extruder to absolute mode
M107       ;start with the fan off
;Put printing message on LCD screen
M117 Testing Auto OFF
M104 S0                     ;extruder heater off
M140 S0                     ;heated bed heater off (if you have it)


M117 Waiting cooldown
M106 S255         ; Activate blower fan
M109 R40.000000          ;wait fior  temperature to reach 40
M190 R40.000000
M106 S0           ; shut down blower fan
M81                  ;ShutDown PSU
M117  Shutdown
