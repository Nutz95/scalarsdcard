M109 S200.000000   ;heat hot end
G21 
M82
M107
G92 E0            ; initialize Extruder to position 0
G1 F500 E30       ; Extrude 30mm of filament speed 500
G1 F1000 E-150    ; Retract 150mm of filament speed 1000
G92 E0            ; reset extruder position to 0
