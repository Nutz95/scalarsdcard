--- EN ---
Hi,

Just received a Scalar 3D printer ?
Perfect, now it�s time to play (almost :-)) using  the building instructions of your printer !
Find it online on our website : http://doc.3dmodularsystems.com/assembly-instructions-for-scalar-3d-printer/
Find all the assembly videos directly on our youtube channel : https://www.youtube.com/channel/UCcV7E7uELxV5k2W3i2dFKAw

Have a great 3D printing experience !

--- FR ---
Bonjour,

Vous venez de recevoir une imprimante 3D Scalar ?
Parfait, maintenant � vous de jouer gr�ce � la notice de montage de votre imprimante !
Retrouvez la en ligne sur notre site : http://doc.3dmodularsystems.com/notices-de-montage-pour-imprimantes-3d-scalar/
Retrouvez �galement toutes les vid�os de montage directement sur notre chaine youtube : https://www.youtube.com/channel/UCcV7E7uELxV5k2W3i2dFKAw

On vous souhaite une exp�rience enrichissante et amusante :-)
